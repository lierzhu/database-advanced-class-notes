# 2021-10-10 多语句递归

## 多语句表值函数语法

```
create function <名称>
（
    @xxx 数据类型 （可加默认值）,
    @yyy 数据类型 （可加默认值）
）
returns @result table (id int , name nvarchar(10))
as
begin
 return
end 

;with CTE_A
as
(
	select Id,CategoryName,ParentId from Category where Id=42
	union all
	select c.Id,c.CategoryName,c.ParentId from Category c,CTE_A a
	where c.Id=a.ParentId

)select * from CTE_A
s
go

create function fn_paratmeter
(
    @name nvarchar(10)
)
returns @result table (Id int, Name nvarchar(20))
as
begin
declare @x nvarchar(80)
	insert into @res(Id,Name) select id,studentname from StudentInfo where studentname=@name

	select @x=name from @res

	delete from @res
	insert into @res(Id,Name) select id,studentname from StudentInfo
		
	drop @res
	alter @res()
	return
end
go
select * from dbo.fn_StudentInfoSelectByName('赵雷')

```

## 视图 

```
go
alter view vw_StudentInfo
as
select a.Id,b.studentname,c.coursename,a.score from studentcoursescore a
left join studentinfo b on a.studentid=b.id
left join CourseInfo c on a.courseId=c.id
select * from vw_StudentInfo
```


## 总结：单语句表值函数和多语句表值函数，实际上的区别只在于后者有一个表类型的变量，在后者定义的函数内部，可以任意的对这个变量
##	进行增删改查的操作(可以操作其中的数据，但是不能对表变量进行删除等操作)；单语句表值函数，并没有一个表类型的变量，它最后只
##	是返回一个表，也可以理解为其整个的过程就是一个的select语句（并不只是说，只能有一个select语句，比如使用CTE，就不只一个select）

###	另外：多语句表值函数中的表变量，类似于数据库当中的临时表

;with CTE_A
AS
(
	select Id,AreaName,ParentCode from Area where ParentCode=56
	union all 
	select b.Id,b.AreaName,b.ParentCode from Area a ,CTE_A b where a.ParentCode=b.Id
)select * from CTE_A
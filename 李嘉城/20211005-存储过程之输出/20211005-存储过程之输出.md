# 2021-10-05 存储过程之输出

## 存储过程输出

```
-- 存储过程相对完整的语法
-- 通常中括号代表可选、而尖括号代表必填

create proc <存储过程名称>
[
    @XXX 数据类型 [=默认值]
]
as
begin
    sql 语句

end

``` 


## 存储过程的返回值

```
-- 语法 单个

create proc proc_StudentInfo
@code nvarchar(80) output
as
begin
	select @code
	select @code=StudentCode from StudentInfo where Id=1
	select @code
end

go

declare @anyCode nvarchar(80)='我和我的祖国'
exec proc_StudentInfo @anyCode
select @anyCode

go
```


## 修改存储过程语法
```
alter proc proc_StudentInfo
@code nvarchar(80) output
as
begin
	select @code
	select @code=StudentCode from StudentInfo where Id=1
	select @code
end
```

## 删除存储过程
```
go
-- 错误的删除命令
delete from proc_StudentInfo

go
-- 正确的删除存储过程的命令
drop proc proc_StudentInfo

go
```

## 多返回值
```
create proc proc_StudentInfo
@code nvarchar(80) output,
@name nvarchar(80) output
as
begin
	select @code,@name
	select @code=StudentCode,@name=StudentName from StudentInfo where Id=1
	select @code,@name
end


go

declare @anyCode nvarchar(80),@anyName nvarchar(80)
exec proc_StudentInfo @anyCode output,@anyName output
select @anyCode,@anyName
go
```

## 自定函数之标量函数 语法
```
create function <方法名称>
(
	[
		@XXXX 数据类型 [=默认值],
		@YYYY 数据类型 [=默认值]
	]
)
returns int -- 表明返回会值是一个int的值
as
begin
	
	declare @result int

	set @result=1

	return @result

end
go

create function fn_GetInt
(

)
returns int -- 表明返回会值是一个int的值
as
begin
	
	declare @result int

	set @result=1

	return @result

end

go


```

-- 如何执行一个标题函数

-- 下面这个对不对？
select fn_GetInt()
use master
go 

create database MKarketVIP_System
go

use MKarketVIP_System
go

create table Market
(
	ScId int primary key identity(1,1) not null,
	ScName nvarchar(50) not null,
	ScFrName nvarchar(20) not null,
	ScAdress nvarchar(100) not null,
	ScNumber nvarchar(50) not null
)
go

create table Memberinformation
(
	HyId int primary key identity(1,1) not null,
	HyNmae nvarchar(20) not null,
	HyNumber nvarchar(50) not null,
	HySex int not null,
	HyBirthday datetime,
)
go

create table CardInformation
(
	HyId int primary key identity(1,1) not null,
	HyNmae nvarchar(20) not null,
	HyNumber nvarchar(50) not null,
	HyAdrees nvarchar(50) not null
)
go

create table VehicleInformation
(
	HyId int primary key identity(1,1) not null,
	HyNumber nvarchar(50) not null,
	HyCarNumber nvarchar(20) not null
)
go

 create table LossInformation
(
	HyId int primary key identity(1,1) not null,
	HyNmae nvarchar(20) not null,
	HyNumber nvarchar(50) not null,
	LossTime datetime,
)
go

create table CardLevelInformation
(
	HyId int primary key identity(1,1) not null,
	HyNmae nvarchar(20) not null,
	HyNumber nvarchar(50) not null,
	Leve nvarchar(10) not null,
)
go

create table ActivityInformation
(
	HdId int primary key not null,
	HdTime datetime,
	HdForPeople nvarchar(50) not null,
	HdContent nvarchar(50) not null,
	HdPerson nvarchar(20) not null,
	HdNumber nvarchar(50) not null,
)
go

create table ShopInformation
(
	SpId int primary key not null,
	SpName nvarchar(50) not null,
	SpPerson nvarchar(20) not null,
	SpNumber nvarchar(50) not null,
	SpQualifications nvarchar(20) not null,
)
go
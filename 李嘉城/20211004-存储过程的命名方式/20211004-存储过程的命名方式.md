# 2021-10-04 存储过程


## 存储过程
```
/*
create proc 名称
as
begin

end
*/

create proc proc_nameow
@name nvarchar(80),
@id int
as
begin
	select*from student
	where name like @name
end
go
exec proc_nameow '1','1..'
```

## 存储过程命名方式

sql 存储过程命名规范
 
句法：

存储过程的命名有这个的语法：
[proc] [MainTableName] By [FieldName(optional)] [Action]
[ 1 ]             [2]                      [3]                      [4]                    

[1] 所有的存储过程必须有前缀“proc_”，所有的系统存储过程都有前缀“sp_”。

注释：假如存储过程以sp_ 为前缀开始命名那么会运行的稍微的缓慢，这是因为SQL Server将首先查找系统存储过程。

[2] 表名就是存储过程主要访问的对象。

[3] 可选字段名就是条件子句。比如： proc_UserInfoByUserIDSelect

[4] 最后的行为动词就是存储过程要执行的任务。

如果存储过程返回一条记录那么后缀是：Select
如果存储过程插入数据那么后缀是：Insert
如果存储过程更新数据那么后缀是：Update
如果存储过程有插入和更新那么后缀是：Save
如果存储过程删除数据那么后缀是：Delete
如果存储过程更新表中的数据 (ie. drop and create) 那么后缀是：Create
如果存储过程返回输出参数或0，那么后缀是：Output


## 存储过程最大参数

```
https://docs.microsoft.com/zh-cn/sql/sql-server/maximum-capacity-specifications-for-sql-server?redirectedfrom=MSDN&view=sql-server-ver15
```

存储过程最大参数数量：2100
存储过程中最大的本地变量数量没有固定限制，由可用内存决定
存储过程最大可用内存为128M
